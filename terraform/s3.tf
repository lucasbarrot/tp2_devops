# TODO : Create a s3 bucket with aws_s3_bucket

resource "aws_s3_bucket" "bucket_web_hosting" {
  bucket = var.s3_user_bucket_name
  force_destroy = true
}

# TODO : Create 1 nested folder :  job_offers/raw/  |  with  aws_s3_bucket_object

resource "aws_s3_bucket_object" "bucket_object" {
  bucket = aws_s3_bucket.bucket_web_hosting.bucket
  key    = "job_offers/raw/"
  source = "/dev/null"
}

# TODO : Create an event to trigger the lambda when a file is uploaded into s3 with aws_s3_bucket_notification

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.bucket_web_hosting.bucket

  lambda_function {
    lambda_function_arn = aws_lambda_function.test_lambda.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "job_offers/raw/"
    filter_suffix       = ".csv"
  }

  depends_on = [aws_lambda_permission.allow_bucket]
}